const engine = class Engine {
    constructor() {
        this.$grid = document.querySelector('.game__grid');
        this.$result = document.querySelector('.game__result');
        this.$score = document.querySelector('.score__count');

        this.squares = [];
        this.width = 4;
        this.score = 0;
        this.startingNumber = 2;

        this.gameEnded = false;

        this.init();
    }

    init() {
        this.createBoard();

        this.generateTwoRandomNumbers();

        this.colorize();

        document.addEventListener('keyup', this.handleKeydown.bind(this));
    }

    createBoard() {
        const squaresCount = this.width * this.width;
        Array(squaresCount).fill(squaresCount).forEach(() => {
            const square = document.createElement('div');
            square.innerHTML = 0;
            this.$grid.appendChild(square);
            this.squares.push(square);
        });
    }

    generateTwoRandomNumbers() {
        Array(2).fill(2).forEach(() => this.generateRandomNumber());
    }

    generateRandomNumber() {
        if (this.gameEnded) {
            return;
        }

        const randomNumber = Math.floor(Math.random() * this.squares.length);
        if (this.squares[randomNumber].innerHTML === '0') {
            this.squares[randomNumber].innerHTML = this.startingNumber;
            this.checkGameOver();
        } else {
            this.generateRandomNumber();
        }
    }

    handleKeydown(e) {
        if (this.gameEnded) {
            return;
        }

        switch (e.key) {
            case 'ArrowLeft':
                this.keyLeftEvent();
                break;
            case 'ArrowRight':
                this.keyRightEvent();
                break;
            case 'ArrowUp':
                this.keyUpEvent();
                break;
            case 'ArrowDown':
                this.keyDownEvent();
                break;
            default:
                break;
        }
    }

    keyLeftEvent() {
        this.moveSquaresHorizontal('left');
        this.combineRow();
        this.moveSquaresHorizontal('left');
        this.generateTwoRandomNumbers();
        this.colorize();
    }

    keyRightEvent() {
        this.moveSquaresHorizontal('right');
        this.combineRow();
        this.moveSquaresHorizontal('right');
        this.generateTwoRandomNumbers();
        this.colorize();
    }

    keyUpEvent() {
        this.moveSquaresVertical('up');
        this.combineColumn();
        this.moveSquaresVertical('up');
        this.generateTwoRandomNumbers();
        this.colorize();
    }

    keyDownEvent() {
        this.moveSquaresVertical('down');
        this.combineColumn();
        this.moveSquaresVertical('down');
        this.generateTwoRandomNumbers();
        this.colorize();
    }

    // left / right
    moveSquaresHorizontal(direction) {
        for (let index=0; index < 16; index++) {
            if (index % 4 === 0) {
                const squaresIndexes = Array(this.width)
                    .fill(this.width)
                    .reduce((arr, current, currentIndex) => {
                        arr.push(currentIndex + index);

                        return arr;
                    }, []);

                const values = squaresIndexes
                    .map(squareIndex => {
                        return parseInt(this.squares[squareIndex].innerHTML);
                    })

                const filteredValues = values.filter(num => num);
                const missingSquares = 4 - filteredValues.length;
                const zeros = Array(missingSquares).fill(0);

                const newValues = direction === 'left' ? filteredValues.concat(zeros) : zeros.concat(filteredValues);

                newValues.forEach((value, i) => {
                    this.squares[squaresIndexes[i]].innerHTML = value;
                });
            }
        }
    }

    // up / down
    moveSquaresVertical(direction) {
        for (let index = 0; index < 4; index++) {
            const squaresIndexes = Array(this.width)
                .fill(this.width)
                .reduce((arr, current, currentIndex) => {
                    arr.push(currentIndex === 0 ? index : index + currentIndex * this.width);

                    return arr;
                }, []);

            const values = squaresIndexes
                .map(squareIndex => {
                    return parseInt(this.squares[squareIndex].innerHTML);
                })

            const filteredValues = values.filter(num => num);
            const missingSquares = 4 - filteredValues.length;
            const zeros = Array(missingSquares).fill(0);

            const newValues = direction === 'up' ? filteredValues.concat(zeros) : zeros.concat(filteredValues);

            newValues.forEach((value, i) => {
                this.squares[squaresIndexes[i]].innerHTML = value;
            });
        }
    }

    combineRow() {
        this.squares.forEach((square, index) => {
            if (index === this.squares.length - 1) {
                return;
            }
            const nextSquare = this.squares[index + 1];
            if (square.innerHTML === nextSquare.innerHTML) {
                const combinedTotal = parseInt(square.innerHTML) + parseInt(nextSquare.innerHTML);
                square.innerHTML = combinedTotal;
                nextSquare.innerHTML = 0;

                this.score += combinedTotal;
                this.$score.innerHTML = this.score;
            }

            this.checkGameWin();
        });
    }

    combineColumn() {
        for (let index = 0; index < 12; index++) {
            const square = this.squares[index];
            const nextSquare = this.squares[index + this.width];
            if (square.innerHTML === nextSquare.innerHTML) {
                const combinedTotal = parseInt(square.innerHTML) + parseInt(nextSquare.innerHTML);
                square.innerHTML = combinedTotal;
                nextSquare.innerHTML = 0;

                this.score += combinedTotal;
                this.$score.innerHTML = this.score;
            }

            this.checkGameWin();
        };
    }

    checkGameOver() {
        let emptySquares = 0;
        this.squares.forEach(square => {
            if (square.innerHTML === '0') {
                emptySquares++;
            }
        });

        if (emptySquares === 0) {
            this.$result.innerHTML = 'Ви програли!';
            this.gameEnded = true;
        }
    }

    checkGameWin() {
        this.squares.forEach((square) => {
            if (square.innerHTML === '2048') {
                this.$result.innerHTML = 'Ви виграли!';
                this.gameEnded = true;
            }
        });
    }

    colorize() {
        this.squares.forEach(square => {
            switch (parseInt(square.innerHTML)) {
                case (2):
                    square.style.backgroundColor = '#eee4da'
                    break;
                case (4):
                    square.style.backgroundColor = '#ede0c8'
                    break;
                case (8):
                    square.style.backgroundColor = '#f2b179'
                    break;
                case (16):
                    square.style.backgroundColor = '#ffcea4'
                    break;
                case (32):
                    square.style.backgroundColor = '#e8c064'
                    break;
                case (64):
                    square.style.backgroundColor = '#ffab6e'
                    break;
                case (128):
                    square.style.backgroundColor = '#fd9982'
                    break;
                case (256):
                    square.style.backgroundColor = '#ead79c'
                    break;
                case (512):
                    square.style.backgroundColor = '#76daff'
                    break;
                case (1024):
                    square.style.backgroundColor = '#beeaa5'
                    break;
                case (2048):
                    square.style.backgroundColor = '#d7d4f0'
                    break;
                default:
                    square.style.backgroundColor = '#afa192'
                    break;
            }
        });
    }
}

new engine();
